
DROP TABLE IF EXISTS tbPessoa CASCADE;
CREATE TABLE tbPessoa (
    idPessoa BIGSERIAL NOT NULL UNIQUE PRIMARY KEY,
    idLogin BIGINT
);

DROP TABLE IF EXISTS tbEmail CASCADE;
CREATE TABLE tbEmail (
    IdEmail BIGSERIAL NOT NULL UNIQUE PRIMARY KEY,
    Email VARCHAR(70),
    Principal BIT,
    IdPessoa BIGINT,
    CONSTRAINT IdPessoa_fk FOREIGN KEY (IdPessoa) REFERENCES tbpessoa(IdPessoa)
);
CREATE UNIQUE INDEX CONCURRENTLY email_index_unique ON tbEmail(Email);
ALTER TABLE tbEmail ADD CONSTRAINT email_unique UNIQUE USING INDEX email_index_unique;

DROP TABLE IF EXISTS tbTelefones CASCADE;
CREATE TABLE tbTelefones (
    IdTelefone BIGSERIAL NOT NULL UNIQUE PRIMARY KEY,
    CodigoGlobal INT DEFAULT 55,
    CodigoRegionalDDD INT,
    Numero INT ,
    Fixo BIT,
    Movel BIT
);
CREATE UNIQUE INDEX CONCURRENTLY numero_completo_unique ON tbtelefones (Numero, CodigoRegionalDDD, CodigoGlobal);
ALTER TABLE tbtelefones ADD CONSTRAINT numero_completo UNIQUE USING INDEX numero_completo_unique;

DROP TABLE IF EXISTS tbTipoTelefone CASCADE;
CREATE TABLE tbTipoTelefone (
    IdTipoTelefone SERIAL NOT NULL UNIQUE PRIMARY KEY,
    TipoEndereco VARCHAR(70)
);
CREATE UNIQUE INDEX CONCURRENTLY index_tipo_end_unique ON tbTipoTelefone (TipoEndereco);
ALTER TABLE tbTipoTelefone ADD CONSTRAINT tipo_end_unique UNIQUE USING INDEX index_tipo_end_unique;

DROP TABLE IF EXISTS tbTelefonesPessoais CASCADE;
CREATE TABLE tbTelefonesPessoais (
    IdPessoa BIGINT  NOT NULL,
    IdTelefone BIGINT NOT NULL,
    IdTipoTelefone BIGINT ,
    PRIMARY KEY (IdPessoa, IdTelefone, IdTipoTelefone),
    CONSTRAINT IdPessoa_fk FOREIGN KEY (IdPessoa) REFERENCES tbpessoa(IdPessoa),
    CONSTRAINT IdTelefone_fk FOREIGN KEY (IdTelefone) REFERENCES tbtelefones(IdTelefone),
    CONSTRAINT IdTipoTelefone_fk FOREIGN KEY (IdTipoTelefone) REFERENCES tbtipotelefone(IdTipoTelefone)
);

DROP TABLE IF EXISTS tbTipoDocumento CASCADE;
CREATE TABLE tbTipoDocumento (
    IdTipoDocumento SERIAL NOT NULL UNIQUE PRIMARY KEY,
    TipoDocumento VARCHAR(70)
);
CREATE UNIQUE INDEX CONCURRENTLY index_documento_unique ON tbTipoDocumento (TipoDocumento);
ALTER TABLE tbTipoDocumento ADD CONSTRAINT doc_unique UNIQUE USING INDEX index_documento_unique;

DROP TABLE IF EXISTS tbDocumentos CASCADE;
CREATE TABLE tbDocumentos (
    IdDocumento BIGSERIAL NOT NULL UNIQUE PRIMARY KEY,
    Documento VARCHAR(70),
    IdPessoa BIGINT,
    IdTipoDocumento INT,
    CONSTRAINT IdPessoa_fk FOREIGN KEY (IdPessoa) REFERENCES tbpessoa(IdPessoa),
    CONSTRAINT IdTipoDocumento_fk FOREIGN KEY (IdTipoDocumento) REFERENCES tbtipodocumento(IdTipoDocumento)
);

DROP TABLE IF EXISTS tbDadosPessoaisPF CASCADE;
CREATE TABLE tbDadosPessoaisPF (
    IdPessoa BIGINT NOT NULL UNIQUE PRIMARY KEY,
    Nome VARCHAR(70),
    NacionalIdade VARCHAR(30),
    DataNascimento TIMESTAMP(3) WITHOUT TIME ZONE ,
    DataCadastro TIMESTAMP(3) WITHOUT TIME ZONE ,
    FotoURL VARCHAR(255),
    CONSTRAINT IdPessoa_fk FOREIGN KEY (IdPessoa) REFERENCES tbpessoa(IdPessoa)
);

DROP TABLE IF EXISTS tbDadosPessoaisPJ CASCADE;
CREATE TABLE tbDadosPessoaisPJ (
    IdPessoa BIGINT NOT NULL UNIQUE PRIMARY KEY,
    RazaoSocial VARCHAR (130),
    NomeFantasia VARCHAR (100),
    NacionalIdade VARCHAR (30),
    DataCadastro TIMESTAMP (3) WITHOUT TIME ZONE,
    LogoURL VARCHAR (255),
    Base VARCHAR (255),
    Segmento VARCHAR (80),
    CONSTRAINT IdPessoa_fk FOREIGN KEY (IdPessoa) REFERENCES tbpessoa(IdPessoa)
);

DROP TABLE IF EXISTS tbEndereco CASCADE;
CREATE TABLE tbEndereco (
    IdEndereco BIGSERIAL NOT NULL UNIQUE PRIMARY KEY,
    Logradouro VARCHAR (70),
    Numero VARCHAR (20),
    Complemento VARCHAR (40),
    Cep VARCHAR (9),
    CIdade VARCHAR (40),
    Estado VARCHAR (2),
    Pais VARCHAR (40)
);

DROP TABLE IF EXISTS tbTipoEndereco CASCADE;
CREATE TABLE tbTipoEndereco (
    IdTipoEndereco SERIAL NOT NULL UNIQUE PRIMARY KEY,
    TipoEndereco VARCHAR (40)
);
CREATE UNIQUE INDEX CONCURRENTLY index_endereco_unique ON tbTipoEndereco (TipoEndereco);
ALTER TABLE tbTipoEndereco ADD CONSTRAINT end_unique UNIQUE USING INDEX index_endereco_unique;

DROP TABLE IF EXISTS tbEnderecosPessoais CASCADE;
CREATE TABLE tbEnderecosPessoais (
    IdPessoa BIGINT NOT NULL,
    IdDadosEnderecos BIGINT  NOT NULL,
    IdTipoEndereco INT ,
    PRIMARY KEY (IdPessoa, IdDadosEnderecos, IdTipoEndereco),
    CONSTRAINT IdPessoa_fk FOREIGN KEY (IdPessoa) REFERENCES tbpessoa(IdPessoa),
    CONSTRAINT IdDadosEnderecos_fk FOREIGN KEY (IdDadosEnderecos) REFERENCES tbendereco(IdEndereco),
    CONSTRAINT IdTipoEndereco_fk FOREIGN KEY (IdTipoEndereco) REFERENCES tbtipoendereco(IdTipoEndereco)
);

DROP TABLE IF EXISTS tbSocialInfo CASCADE;
CREATE TABLE tbSocialInfo (
    IdSocialInfo BIGSERIAL NOT NULL UNIQUE PRIMARY KEY,
    FacebookHook VARCHAR (500),
    GoogleHook VARCHAR (500),
    IdPessoa BIGINT,
    CONSTRAINT fk_IdPessoa FOREIGN KEY (IdPessoa) REFERENCES tbpessoa(IdPessoa)
);

DROP TABLE IF EXISTS tbContatos CASCADE;
CREATE TABLE tbContatos (
    IdPessoaPrincipal BIGINT  NOT NULL,
    IdPessoaContato BIGINT NOT NULL,
    PRIMARY KEY (IdPessoaPrincipal, IdPessoaContato),
    CONSTRAINT IdPessoaPrincipal_fk FOREIGN KEY (IdPessoaPrincipal) REFERENCES tbpessoa(IdPessoa),
    CONSTRAINT IdPessoaContato_fk FOREIGN KEY (IdPessoaContato) REFERENCES tbpessoa(IdPessoa)
);



DROP TABLE IF EXISTS tbDisponibilIdade CASCADE;
CREATE TABLE tbDisponibilIdade
(
    IdDisponibilIdade BIGSERIAL NOT NULL UNIQUE PRIMARY KEY,
    Ckin TIME,
    Ckot TIME,
    QtdAps INT,
    JantarSS BIT,
    JantarNS BIT,
    VlrNCS DECIMAL,
    VlrCRS DECIMAL,
    QtdAPD INT,
    JantarSD BIT,
    JantarND BIT,
    VlrNCD DECIMAL,
    VlrCRD DECIMAL,
    DtIn DATE,
    DtOt DATE,
    QtdExec INT,
    VlrExec DATE,
    Estrelas INT,
    IdPessoaHotel BIGINT,
    CONSTRAINT IdPessoaHotel_fk FOREIGN KEY (IdPessoaHotel) REFERENCES tbpessoa(IdPessoa)
);

DROP TABLE IF EXISTS tbReservas CASCADE;
CREATE TABLE tbReservas
(
    IdReserva BIGSERIAL NOT NULL UNIQUE PRIMARY KEY,
    QtdAPS INT,
    IdPessoaAeroporto INT,
    JantarSS BIT,
    JantarSD BIT,
    QtdAPD INT,
    QtdExec INT,
    JantarNS BIT,
    JantarND BIT,
    Conf BIT,
    IdDisponibilIdade BIGINT,
    IdPessoaHospede BIGINT,
    CONSTRAINT IdReservas_fk FOREIGN KEY (IdDisponibilIdade) REFERENCES tbDisponibilIdade(IdDisponibilIdade),
    CONSTRAINT IdPessoaHospede_fk FOREIGN KEY (IdPessoaHospede) REFERENCES tbpessoa(IdPessoa)
);

DROP TABLE IF EXISTS tbApartamentos CASCADE;
CREATE TABLE tbApartamentos
(
    IdApartamento BIGSERIAL NOT NULL UNIQUE PRIMARY KEY,
    ap VARCHAR(50),
    IdReserva BIGINT,
    CONSTRAINT Idreserva_fk FOREIGN KEY (IdReserva) REFERENCES tbReservas(IdReserva)
);

DROP TABLE IF EXISTS tbConfiguracoes CASCADE;
CREATE TABLE tbConfiguracoes
(
    IdConfiguracao SERIAL NOT NULL UNIQUE PRIMARY KEY,
    IdConfiguracaoPai INT,
    Chave VARCHAR(100),
    Valor VARCHAR(200),
    Ativo BIT
);

--GRANT CONNECT ON DATABASE Maximianos TO application;
--GRANT SELECT, INSERT, UPDATE, DELETE, TRUNCATE, REFERENCES, TRIGGER ON ALL TABLES IN SCHEMA public TO application;
--GRANT USAGE, SELECT ON ALL SEQUENCES IN SCHEMA public TO application;

ALTER TABLE tbtipotelefone CLUSTER ON tbtipotelefone_pkey;
ALTER TABLE tbtipoendereco CLUSTER ON tbtipoendereco_pkey;
ALTER TABLE tbtipodocumento CLUSTER ON tbtipodocumento_pkey;
ALTER TABLE tbtelefonespessoais CLUSTER ON tbtelefonespessoais_pkey;
ALTER TABLE tbtelefonespessoais CLUSTER ON tbtelefonespessoais_pkey;
ALTER TABLE tbtelefonespessoais CLUSTER ON tbtelefonespessoais_pkey;
ALTER TABLE tbtelefones CLUSTER ON tbtelefones_pkey;
ALTER TABLE tbsocialinfo CLUSTER ON tbsocialinfo_pkey;
ALTER TABLE tbreservas CLUSTER ON tbreservas_pkey;
ALTER TABLE tbpessoa CLUSTER ON tbpessoa_pkey;
--ALTER TABLE tbpermissoesdoperfil CLUSTER ON tbpermissoesdoperfil_pkey;
--ALTER TABLE tbpermissoesdoperfil CLUSTER ON tbpermissoesdoperfil_pkey;
--ALTER TABLE tbpermissao CLUSTER ON tbpermissao_pkey;
--ALTER TABLE tbperfildologin CLUSTER ON tbperfildologin_pkey;
--ALTER TABLE tbperfildologin CLUSTER ON tbperfildologin_pkey;
--ALTER TABLE tbperfil CLUSTER ON tbperfil_pkey;
--ALTER TABLE tblogin CLUSTER ON tblogin_pkey;
ALTER TABLE tbenderecospessoais CLUSTER ON tbenderecospessoais_pkey;
ALTER TABLE tbenderecospessoais CLUSTER ON tbenderecospessoais_pkey;
ALTER TABLE tbenderecospessoais CLUSTER ON tbenderecospessoais_pkey;
ALTER TABLE tbendereco CLUSTER ON tbendereco_pkey;
ALTER TABLE tbemail CLUSTER ON tbemail_pkey;
ALTER TABLE tbdocumentos CLUSTER ON tbdocumentos_pkey;
ALTER TABLE tbdisponibilidade CLUSTER ON tbdisponibilidade_pkey;
ALTER TABLE tbdadospessoaispj CLUSTER ON tbdadospessoaispj_pkey;
ALTER TABLE tbdadospessoaispf CLUSTER ON tbdadospessoaispf_pkey;
ALTER TABLE tbcontatos CLUSTER ON tbcontatos_pkey;
ALTER TABLE tbcontatos CLUSTER ON tbcontatos_pkey;
ALTER TABLE tbconfiguracoes CLUSTER ON tbconfiguracoes_pkey;
ALTER TABLE tbapartamentos CLUSTER ON tbapartamentos_pkey;


--ALTER TABLE tbPessoa CLUSTER ON tbpessoa_pkey;


--CONFERIR SE TEM CLUSTERED INDEX
SELECT relname AS table_name
FROM   pg_class c
JOIN   pg_index i ON i.indrelid = c.oid
WHERE  relkind = 'r' AND relhasindex AND i.indisclustered;

--SCRIPT PARA CRIAR SCRIPTS DE CLUSTERED INDEX
select concat('ALTER TABLE ', kcu.table_name, ' CLUSTER ON ', tco.constraint_name, ';'),
       kcu.table_schema,
       kcu.table_name,
       tco.constraint_name,
       kcu.ordinal_position as position,
       kcu.column_name as key_column
from information_schema.table_constraints tco
join information_schema.key_column_usage kcu
     on kcu.constraint_name = tco.constraint_name
     and kcu.constraint_schema = tco.constraint_schema
     and kcu.constraint_name = tco.constraint_name
where tco.constraint_type = 'PRIMARY KEY'
order by kcu.table_schema,
         kcu.table_name,
         position;


/*
//CRIAÇÃO DE INDICES
DROP INDEX IF EXISTS idx_pessoa CASCADE;
CREATE INDEX idx_pessoa ON tbpessoa using btree(IdPessoa);
ALTER TABLE tbpessoa CLUSTER ON idx_pessoa;
*/



